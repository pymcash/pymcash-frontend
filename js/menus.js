$(function(){
//Menu tablet - movil
	var elemento = $('a.menu');
	elemento.on('click',function(e){
	     $('a.menu').removeClass("linea");
	     $(this).addClass("linea");
	     e.preventDefault();
	});

//Menu pc
	var element2 = $('a.icono');
	element2.on('click',function(e){
		$('a.icono').removeClass("linea2");
		$(this).addClass("linea2");
		e.preventDefault();
	});

//Menu desplegable pantalla grande
	var boton = $('#tuerca');
	var fondo_enlace = $('#fondo-enlace');

	boton.on('click', function(e){
		fondo_enlace.toggleClass('active');
		$('#barra-lateral-izquierda').toggleClass('active');
		e.preventDefault();
	});

	fondo_enlace.on('click',function(e){
		fondo_enlace.toggleClass('active');
		$('#barra-lateral-izquierda').toggleClass('active');
		e.preventDefault();
	});

//Menu desplegable pantalla móvil/tablet
	var boton2 = $('#tuerca2');
	var fondo_enlace2 = $('#fondo-enlace2');

	boton2.on('click', function(e){
		fondo_enlace2.toggleClass('active');
		$('#barra-lateral-derecha').toggleClass('active');
		e.preventDefault();
	});

	fondo_enlace2.on('click',function(e){
		fondo_enlace2.toggleClass('active');
		$('#barra-lateral-derecha').toggleClass('active');
		e.preventDefault();
	});

//Menu chat bloquear/vaciarchat
	var boton3 = $('#tuercachat');
	var fondo_enlace3 = $('#fondo-enlace-chat');

	boton3.on('click', function(e){
		fondo_enlace3.toggleClass('active');
		$('#barra-lateral-chat').toggleClass('active');
		e.preventDefault();
	});

	fondo_enlace3.on('click',function(e){
		fondo_enlace3.toggleClass('active');
		$('#barra-lateral-chat').toggleClass('active');
		e.preventDefault();
	});


}())