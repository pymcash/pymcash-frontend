<?php require 'head.php'; ?>
	<div class="header-billetera">
		<section class="money-user">
			<div class="contenedor">
				<div class="icono-izquierda">
					<a href="#" class="icono-izquierda mt-2"><i class="icon-left-open"></i></a>
				</div>
				<div class="texto">
					<h3 class="saldo">Saldo</h3>
					<p class="monto">$2000</p>
					<p class="info">Recuerde que sólo podrá canjear a partir de $1000</p>
				</div>
			</div>
		</section>
	</div>
	<section class="main-billetera">
		<div class="boton-canjear">
			<div class="contenedor">
				<button class="btn boton-verde" type="button"><a href="#">Movimientos</a></button>
			</div>
		</div>

		<div class="container">
			<div class="row datos">
				<p class="texto col-12">Datos para la transferencia</p>
				<form class="col-12 datosbancarios" action="" method="post">
					<!-- RUT usuario -->
					<input type="text" class="form-control" value="" placeholder="RUT"
					>
				    <!-- Entidad Bancaria -->
				    <select class="form-control" id="Banco">
				      <option>Banco</option>
				      <option>BANCOESTADO</option>
				      <option>BANCO INTERNACIONAL</option>
				      <option>SCOTIABANK CHILE</option>
				      <option>BANCO DE CREDITO E INVERSIONES</option>
				      <option>CORPBANCA</option>
				      <option>BANCO BICE</option>
				      <option>HSBC BANK (CHILE)</option>
				      <option>BANCO SANTANDER-CHILE</option>
				      <option>BANCO ITAU CHILE</option>
				      <option>THE BANK OF TOKYO-MITSUBISHI UFJ</option>
				      <option>BANCO SECURITY</option>
				      <option>BANCO FALABELLA</option>
				      <option>BANCO RIPLEY</option>
				      <option>BANCO CONSORCIO</option>
				      <option>BANCO PARIS</option>
				      <option>BANCO BILBAO VIZCAYA ARGENTARIA</option>
					  <option>COOPEUCH</option>
					  <option>BANCO DE CHILE</option>
				    </select>
					<!-- Tipo de cuenta -->
					 <select class="form-control" id="tipocuenta">
				      <option>Cuenta Vista</option>
				      <option>Cuenta de ahorro</option>
				      <option>Cuenta Corriente</option>
				      <option>Cuenta RUT</option>
				    </select>
				    <!-- Numero de cuenta usuario -->
					<input type="text" class="form-control" value="" placeholder="Numero de cuenta"
					>
					<!-- EMAIL usuario -->
					<input type="email" class="form-control" value="" placeholder="Email"
					>

					<button class="btn boton-verde" name="canjear" type="submit" value="canjear">Canjear</button>
				</form>
			</div>
		</div>
	</section>
<?php require 'footer.php'; ?>