<?php require 'header2.php'; ?>		
	<div class="main-promo">
		
		<div class="titulo">
			<p>Lista de Miembros</p>
		</div>
		
		    <form class="contenedor" method="post" action="promocion.php">
		        <div class="group-checkall">
		        	<div class="texto">
		        		<p>Seleccionar todos</p>
		        	</div>
		        	<div class="input-checkall">
		        		<input type="checkbox" onClick="toggle(this)" value="">
		        	</div>
		        </div>
				<div class="group-checkbox" id="contactos" name="contactos">
					<!-- Contacto #1 -->
			        <div class="contacto">
			            <div class="imagen">
			    			<img src="../iconos/letras/c.png"> 
			            </div>
			            <div class="nombre">
			        		<p>Cristina Hernández</p>
			            </div>
			            <div class="input-single">
			    			<input type="checkbox" name="checklist[]" value="">
			            </div>
					</div>
		           	<!-- Contacto #2 -->
			        <div class="contacto">
			            <div class="imagen">
			    			<img src="../iconos/letras/g.png"> 
			            </div>
			            <div class="nombre">
			        		<p>Gustavo Dominguez</p>
			            </div>
			            <div class="input-single">
			    			<input type="checkbox" name="checklist[]" value="">
			            </div>
					</div>
					<!-- Contacto #3 -->
					<div class="contacto">
			           <div class="imagen">
			    			<img src="../iconos/letras/j.png"> 
			            </div>
			            <div class="nombre">
			        		<p>John Smith</p>
			            </div>
			            <div class="input-single">
			    			<input type="checkbox" name="checklist[]" value="">
			            </div>
					</div>
					<!-- Contacto #4 -->
					<div class="contacto">
			           <div class="imagen">
			    			<img src="../iconos/letras/o.png"> 
			            </div>
			            <div class="nombre">
			        		<p>Oscar Farias</p>
			            </div>
			            <div class="input-single">
			    			<input type="checkbox" name="checklist[]" value="">
			            </div>
					</div>
					<!-- Contacto #5 -->
					<div class="contacto">
			           <div class="imagen">
			    			<img src="../iconos/letras/o.png"> 
			            </div>
			            <div class="nombre">
			        		<p>Oscar Farias</p>
			            </div>
			            <div class="input-single">
			    			<input type="checkbox" name="checklist[]" value="">
			            </div>
					</div>

				</div>

		        <div class="mensaje">
		        	<div class="textarea">
		            	<textarea  id="msg" name="msg" placeholder="Escriba su promoción"></textarea>
		        	</div>           
		            <div class="boton">
		            	<button type="submit" class="btn button">Enviar promoción</button>
		            </div>
		        </div>
	   		</form>
	</div>

    <script language="JavaScript">
       function toggle(source) {
       checkboxes = document.getElementsByName('checklist[]');
       for(var i=0, n=checkboxes.length;i<n;i++){
       checkboxes[i].checked = source.checked;}}
    </script>

<?php require 'footer2.php'; ?>