<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Comercio PymCash</title>
	</head>
	<body>
		<main>
			<div class="user-commerce">
				<div class="container">
					<div class="row">
						<div class="imagen col-12">
							<img src="../img/logopymcash.png" alt="Logotipo PymCash">
						</div>
						<div class="titulo col-12">
							<p class="h3">Miembro</p>
						</div>

						<div class="form-group col-12">
                        	<i class="icon-user"></i><input class="inputrut" type="text" placeholder="RUT" name="user" id="user" required="required">
						</div>

	                    <div class="form-group col-12">
							<i class="icon-lock"></i><input class="inputpassword" type="password" name="pass" id="pass" required="required" placeholder="Contraseña">
						</div>

	                    <div class="form-group col-12">
							<div class="boton login-button">
								<button type="submit" name="submit" class="boton">
									Entrar
								</button>
							</div>
							<div class="link col-12">
								<a href="#">¿Olvidaste tu contraseña? <strong>Ingresa aquí</strong></a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</main>

		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/menus.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/boostrap.min.js"></script>
	</body>
</html>
