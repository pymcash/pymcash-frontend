<?php require 'header.php'; ?>
	<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda activo">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha">
				  			<a href="#">Mensajería</a>
				  		</button>
					</div>
				</div>
				<div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="commerceblock">
						<a href="#">
							<img src="../iconos/bloquear.png" width="22" height="22">
							<p class="texto">Comercios Bloqueados</p>
						</a>
					</div>
					<div class="deleteall">
						<a href="#">
							<img src="../iconos/eliminar.png" width="22" height="22">
							<p class="texto">Eliminar Todas</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row notificaciones">

					<!-- Notificacion #1 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Responder</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Responder</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
						<!-- Notificacion #2 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Responder</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Responder</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
					<!-- Notificacion #3 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Responder</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Responder</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
					<!-- Notificacion #4 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Responder</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Responder</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>

				</div>
			</div>
		</div>
<?php require 'footer.php'; ?>