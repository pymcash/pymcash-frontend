<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Registro comercio PymCash</title>
	</head>
	<body>
		<header>
			<div class="icono-izquierda">
				<a onclick="window.history.back()" class="icono-izquierda mt-2"><i class="icon-left-open-big"></i></a>
			</div>
				<div class="texto2 text-center">
   		        	<p>Confirme la ubicación de su comercio en el mapa</p>
				</div>
		</header>
		<main>
			<div class="register-commerce-map">
					 	
					  <div id="map"></div>
            
            <form name="myform" class="form-inline text-center" method="post" action="registercomercedo.php">
                <input type="text" id="rut_comercio" name="rut_comercio" style="display:none">
                <input type="text" id="nombre_comercio" name="nombre_comercio" style="display:none">
                <input type="text" name="nombre_fantasia" id="nombre_fantasia" style="display:none">
                <input type="text" name="direccion" id="direccion" style="display:none">
                <input type="text" name="telefono" id="telefono" style="display:none">
                <input type="text" name="email" id="email" style="display:none">
                <input type="text" name="pass" id="pass" style="display:none">
                <input type="text" name="passconfirm" id="passconfirm" style="display:none">
                <input type="text" id="latitud" name="latitud" style="display:none">
                <input type="text" id="longitud" name="longitud" style="display:none">

             <div class="boton-complete">
               <button type="submit" value="Completar Registro" name="submit" class="boton">
                Completar Registro
              </button>
            </div>

                
  
              </form>

		<script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow;
      var markers = [];
      var marker;
      var defpos = {lat: -33.523524, lng: -70.734431};

      function setMarkerPos(){
        marker.setPosition(pos);
      }
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.523524, lng: -70.734431},
          zoom: 14
        });

        // This event listener will call addMarker() when the map is clicked.
		    map.addListener('click', function(event) {
          //addMarker(event.latLng);
        });
        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);

         marker = new google.maps.Marker({
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
          position: {lat: 10.219033, lng: -64.630385},
        });

        marker.addListener('click', toggleBounce);
        marker.addListener('mouseup',UpdatePos);
        marker.setAnimation(google.maps.Animation.BOUNCE);
        UpdatePos();
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };


            marker.setPosition(pos);

            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
        //alert(getMarkerLatitud());
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }

      function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }

      function UpdatePos(){
        document.getElementById("latitud").value = getMarkerLatitud();
        document.getElementById("longitud").value = getMarkerLongitud();
      }

      // Adds a marker to the map and push to the array.
      function addMarker(location) {
      	alert("Alerta: "+location);
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

      function getMarkerLatitud(){
        return marker.getPosition().lat()
      }
      function getMarkerLongitud(){
        return marker.getPosition().lng();
      }



   </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgaWQCCCIBx6I84EJLpH06C0fhyqq-I5E&callback=initMap">
    </script>

      
					
					</form>

					</div>
		</main>
		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/menus.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/boostrap.min.js"></script>
	</body>
</html>
