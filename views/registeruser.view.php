<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Registro usuario PymCash</title>
	</head>
	<body>
		<header>
			<div class="icono-izquierda">
				<a href="#" class="icono-izquierda mt-2"><i class="icon-left-open-big"></i></a>
			</div>
				<div class="texto1 h3 text-center">
   		        	<p>¡Bienvenido!</p>
				</div>
		</header>
		<main>
			<div class="register-user-commerce">
				<div class="container">
					<div class="row col-12">
					 	
					 <form id="main-contact-form" class="formulario col-12" name="contact-form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data" > 
                     
                        <input type='file' id="imgInp" name="imgInp" class="file" style="display:none">
							<div class="text-center imagen">
								<img id="blah" src="../img/icon-user.png" class="img-rounded" alt="Foto de perfil">
							</div>

						<div class="form-group text-center boton">
							<a onclick="document.getElementById('imgInp').click();" class="login1" role="button">Cargar Foto</a>		
						</div>
						
                     	<div class="form-group text-center">
							<input type="text" value="0"  name="lat" id="lat" hidden>
							<input type="text"  value="0"  name="long" id="long" hidden>
                        	<input type="text" pattern=".{6,}" required title="minimo 6 caracteres"  name="rut" id="rut" required="required" placeholder="RUT">
                    	</div>
                     
                     	<div class="form-group text-center">
							<input type="text" name="nombre" required="required" placeholder="Nombre">
                    	</div>

                  		<div class="form-group text-center">
				  			<input type="text" name="apellido" id="apellido" required="required" placeholder="Apellido">
                		</div>

                		<div class="form-group text-center">
				 		 <input type="date" name="date" id="date" required="required" value="1994-12-23">
						</div>
    
                     	<div class="form-group text-center">
				  			<input type="text" name="direccion" id="direccion" required="required" placeholder="Direccion">
                  		</div>
                     
               			<div class="form-group text-center">
				  			<input type="text" name="comuna" id="comuna" required="required" placeholder="Comuna">
                   		</div>
                     
                  		 <div class="form-group text-center">
				 			<input type="text" name="telefono" id="telefono" class="input1" required="required" placeholder="Teléfono">
                    	</div>
                     
                   		<div class="form-group text-center">
				 		 <input type="text" name="email" id="email" required="required" placeholder="Email">
						</div>
					
						 <div class="form-group text-center">
							<input type="text" name="referidor" id="referidor" placeholder="Rut Referidor (Si fue invitado)">
						 </div>
                     
	                     <div class="form-group text-center">
							<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="pass" id="pass" required="required" placeholder="Contraseña">
	                     </div> 
	                     
                    	<div class="form-group text-center">
							<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="passconfirm" id="passconfirm" required="required" placeholder="Repita la contraseña">
                     	</div>
						
						<div class="checkbox text-center">
                     		<input type="checkbox" class="checkbox" name="cb-autos" value=""> Acepto los <a href="#">términos y condiciones</a><br>
						</div>
                    
                     	<div class="form-group text-center boton">
                            <button type="submit" value="Registrarme" name="submit">
								Registrarme
							</button>
						</div>
					
					</form>

					</div>
				</div>
			</div>
		</main>
		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/menus.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/boostrap.min.js"></script>
	</body>
</html>
