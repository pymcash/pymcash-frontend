<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet"> 
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Inicio PymCash</title>
	</head>
	<body>
		<main>
			<div class="container">
				<div class="row">
					<div class="imagen col-12">
						<img src="../img/logopymcash.png" alt="Logotipo PymCash">
					</div>
					<div class="texto col-12">
						<p>¡Regístrate Ahora!</p>
					</div>
					<div class="boton col-12">
						<button><a href="#">Crear Cuenta</a></button>
					</div>
					<div class="link col-12">
						<a href="#">¿Ya tienes cuenta? <strong>Ingresa aquí</strong></a>
					</div>
				</div>
			</div>
		</main>

	</body>
</html>
