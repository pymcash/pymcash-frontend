<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Inicio PymCash</title>
	</head>
	<body>
		<main>
			<div class="chooselogin">
				<div class="container">
					<div class="row">
						<div class="titulo col-12">
							<p class="h2">¿Aún no tienes cuenta?</p>
						</div>
						<div class="textocreate col-12">
							<p>¡Cualquiera puede ser miembro de la comunidad!<br> Recuerda que para ser comercio debes tener un local comercial</p>
						</div>
						<div class="boton boton1 col-12">
							<button><a href="#">Deseo ser miembro</a></button>
						</div>
						<div class="boton col-12">
							<button><a href="#">Deseo ser comercio</a></button>
						</div>
						<div class="link col-12">
							<a href="#">¿Ya tienes cuenta? <strong>Ingresa aquí</strong></a>
						</div>
					</div>
				</div>
			</div>
		</main>

	</body>
</html>
