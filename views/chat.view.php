<?php require 'head.php'; ?>
	<body>
		<div class="header">
			<div class="container">
				<div class="row chat-header">
					<div class="col-2 icono-izquierda">
						<a href="#" class="icono-izquierda mt-2"><i class="icon-left-open"></i></a>
					</div>
				    

					<div class="col-2 imagen">
						<img src="../iconos/letras/b.png">
					</div>
					<div class="col-6 titulo">
						<p>Chat con Botillería La Lucy</p>
					</div>
					
					<div class="col-2 icono-derecha">
						<a href="#" id="tuercachat"><img src="../iconos/tuercagris.png" width="22" height="22"></a>
					</div>	
					<!-- Menu desplegable chat -->
				    <div class="col-md-3 barra-lateral-chat" id="barra-lateral-chat">
						<nav>
							<a href="#"><i class="icon-lock"></i>Bloquear Comercio</a>
							<hr>
							<a href="#"><i class="icon-trash-empty"></i>Vaciar Chat</a>
						</nav>
					</div> 
					<!-- Fondo menu -->
					<a href="#" class="fondo-enlace-chat" id="fondo-enlace-chat"></a>
				</div>
			</div>
		</div>
		<div class="chat-history">
			<div class="container">
				<div class="row">
					<ul class="col-12">
						<!-- Mensaje 1 -->
						<li class="clearfix col-12">
							<div class="message-data d-flex justify-content-end">
								<span class="message-data-time">10:10 AM, Hoy</span> &nbsp; &nbsp;
								<span class="message-data-name">Botilleria La Lucy</span>
							</div>
							<div class="message other-message d-flex justify-content-end">
								¡Los invitamos hoy al descuentazo por el mes de mayo!
							</div>
						</li>
						<!-- Mensaje 2 -->
						<li class="col-12">
							<div class="message-data">
								<span class="message-data-name">John Smith</span>
								<span class="message-data-time">10:13 AM, Hoy</span>
							</div>
							<div class="message my-message">
								Hola quisiera un six pack de cerveza y unos cigarrillos, paso en 20 minutos
							</div>
						</li>
						<!-- Mensaje 3 -->
						<li class="clearfix col-12">
				            <div class="message-data d-flex justify-content-end">
				              <span class="message-data-time" >10:15 AM, Hoy</span> &nbsp; &nbsp;
				              <span class="message-data-name" >Botilleria La Lucy</span>
				            </div>
				            <div class="message other-message d-flex justify-content-end">
				              ¡Perfecto John! Te esperamos con tu orden
				            </div>
				        </li>
						<!-- Mensaje 4 -->
				        <li class="col-12">
							<div class="message-data">
								<span class="message-data-name">John Smith</span>
								<span class="message-data-time">10:20 AM, Hoy</span>
							</div>
							<div class="message my-message">
								Muchas gracias, excelente servicio
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
		<div class="chat-messages">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<textarea name="message-to-send" id="message-to-send" placeholder=" Escriba su mensaje" rows="3"></textarea>
					</div>
					<div class="col-12 d-flex justify-content-end">
						<button>ENVIAR</button>
					</div>
				</div>
			</div>
		</div>

	</body>

<?php require 'footer.php';