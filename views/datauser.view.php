<?php require 'head.php'; ?>
	<header>
		<div class="header-datauser">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="#"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-4 col-10">
						Datos Personales
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-datauser">
		<div class="container">
			<div class="row datos">
				
				<form id="main-contact-form col-12" class="contact-form" name="contact-form" method="post" action="#" enctype="multipart/form-data"> 

	                 <div class="imagen col-12">
						<img src="../img/gustavo.jpg" id="DNI" width="200px" name="DNI" alt="Documento de identidad">
					</div>
                  	
                  	<div class="rut">
                        <input type="text" name="rut" id="rut" placeholder="RUT" readonly>
                  	</div>
                    
                    <div class="nombre">
						<input type="text" name="nombre" id="nombre" placeholder="Nombre" readonly>
                    </div>
					
					<div class="apellido">
				 		<input type="text" name="apellido" id="apellido" placeholder="Apellido" readonly>
					</div>

					<div class="direccion">
						<input type="text" name="direccion" id="direccion" class="input100" required="required" placeholder="Direccion">
					</div>
    
	                <div class="comuna">
	                 	<input type="text" name="comuna" id="comuna" class="input100" required="required" placeholder="Comuna">
	                </div>
                     
                     <div class="telefono">
                     	<input type="text" name="telefono" id="telefono" class="input100" required="required" placeholder="Teléfono">
                     </div>
					
					<div class="mail">
						<input type="text" name="email" id="email" class="input100" required="required" placeholder="Email">
					</div>
                     
                	<div class="boton">
                		<button type="submit" name="save" id="save">
						Guardar Cambios
						</button>
                	</div>
                 
				</form>


			</div>
		</div>
	</div>
<?php require 'footer.php'; ?>