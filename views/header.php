<?php require 'head.php'; ?>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<!-- Menu pantalla pc -->
					<div class="col-12 d-none d-md-block menu">
						<nav class="col-sm-5 justify-content-between menu d-flex ml-auto">
							<a href="#" class="icono"><img src="../iconos/homeblanco.png"></a>
							<a href="#" class="icono locate"><img src="../iconos/locatecommerceblanco.png"></a>
							<a href="#" class="icono chat"><img src="../iconos/chatblanco.png"><span class="badge badge-danger">1</span></a>
						</nav>
				    </div>
				    <!-- Menu superior pantalla movil, tablet -->
				    <div class="col-12 d-md-none menu2">
				    	<nav class="mb-2 mt-2 d-flex justify-content-between">
				    		<a href="#" class="icono-izquierda mt-2"><i class="icon-left-open"></i></a>
							<a href="#" class="icono-derecha mt-2" id="tuerca2"><img src="../iconos/tuercablanca.png" width="22" height="22"></a>
						</nav>
				    </div>
					
					<!-- Barra menu onclick -->
				    <div class="col-md-3 barra-lateral-derecha" id="barra-lateral-derecha">
						<nav>
							<a href="#"><i class="icon-user"></i>Ficha Personal</a>
							<a href="#"><i class="icon-switch"></i>Cambiar Contraseña</a>
							<hr>
							<a href="#">Términos y condiciones</a>
							<a href="#">Cerrar Sesión</a>
						</nav>
					</div> 
					<!-- fondo menu -->
					<a href="#" class="fondo-enlace2" id="fondo-enlace2"></a>

				</div>
			</div>
			<!-- Menu inferior pantalla móvil, tablet -->
		   <div class="col-12 d-md-none menu3">
				<nav class="justify-content-around d-flex mb-2 mt-2">
					<a href="#" class="menu"><img src="../iconos/home.png"></a>
					<a href="#" class="menu locate"><img src="../iconos/locatecommerce.png"></a>
					<a href="#" class="menu chat"><img src="../iconos/chat.png"><span class="badge badge-danger notificacion">1</span></a>
				</nav>
			</div>
		</header>