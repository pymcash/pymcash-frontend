<?php require 'head2.php'; ?>
	<header>
		<div class="header-commerce">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="#"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-4 col-10">
						Datos de su comercio
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-datacommerce">
		<div class="container">
			<div class="row datos">
				
				<form id="main-contact-form col-12" class="contact-form" name="contact-form" method="post" action="#" enctype="multipart/form-data"> 

	                 <div class="imagen col-12">
						<img src="../img/MercadoChamartin.jpg" id="DNI" width="200px" name="DNI" alt="Documento de identidad">
					</div>
                  	
                  	<div class="rutcomercio">
                        <input type="text" name="rutcomercio" id="rutcomercio" placeholder="RUT del comercio" readonly>
                  	</div>
                    
                    <div class="razonsocial">
						<input type="text" name="razonsocial" id="razonsocial" placeholder="Razón social del comercio" readonly>
                    </div>
					
					<div class="nombrefantasia">
				 		<input type="text" name="nombrefantasia" id="nombrefantasia" placeholder="Nombre fantasía del comercio" readonly>
					</div>

					<div class="direccion">
						<input type="text" name="direccioncomercio" id="direccioncomercio" required="required" placeholder="Direccion">
					</div>
    
	                <div class="comuna">
	                 	<input type="text" name="comuna" id="comuna" required="required" placeholder="Comuna">
	                </div>
                     
                     <div class="telefono">
                     	<input type="text" name="telefono" id="telefono" required="required" placeholder="Teléfono">
                     </div>
					
					<div class="mail">
						<input type="text" name="email" id="email" required="required" placeholder="Email">
					</div>
                     
                	<div class="boton">
                		<button type="submit" name="save" id="save">
						Guardar Cambios
						</button>
                	</div>
                 
				</form>


			</div>
		</div>
	</div>
<?php require 'footer2.php'; ?>