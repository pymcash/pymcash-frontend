<?php require 'header.php'; ?>
	<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha">
				  			<a href="#">Mensajería</a>
				  		</button>
					</div>
				</div>
				<div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="commerceblock activobloqueados">
						<a href="#">
							<img src="../iconos/bloquear.png" width="22" height="22">
							<p class="texto">Comercios Bloqueados</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row bloqueados">

					<!-- Bloqueado #1 -->
					<div class="bloqueado d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/c.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Comida Rápida El Gran Caimán</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#"><i class="icon-lock-open">Desbloquear</i></a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary"><i class="icon-lock-open">Desbloquear</i></a>
						</div>
					</div>

					<!-- Bloqueado #2 -->
					<div class="bloqueado d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Bazar El Pasillo</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#"><i class="icon-lock-open">Desbloquear</i></a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary"><i class="icon-lock-open">Desbloquear</i></a>
						</div>
					</div>

					<!-- Bloqueado #3 -->
					<div class="bloqueado d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Bazar El Pasillo</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#"><i class="icon-lock-open">Desbloquear</i></a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary"><i class="icon-lock-open">Desbloquear</i></a>
						</div>
					</div>

					<!-- Bloqueado #4 -->
					<div class="bloqueado d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Bazar El Pasillo</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#"><i class="icon-lock-open">Desbloquear</i></a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary"><i class="icon-lock-open">Desbloquear</i></a>
						</div>
					</div>


				</div>
			</div>
		</div>
<?php require 'footer.php'; ?>