<?php require 'header.php'; ?>
	<div class="main-mensajeria">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha activo">
				  			<a href="#">Mensajería</a>
				  		</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row barrabusqueda">
				 <div class="barra input-group col-md-12 d-flex justify-content-center">
	                <input type="text" id="busqueda" name="busqueda" class="form-control barra" placeholder="Buscar contacto" aria-describedby="basic-addon1"
	                >
	                <i class="icon-search"></i>
                </div>
			</div>
		</div>

		<br/>
		<div class="container">
			<div class="row contactos">
				<!-- Contacto #1 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
					<div class="col-2 col-md-4 imagen">
						<img src="../iconos/letras/b.png">
					</div>
					<div class="col-10 col-md-8 content">
						<p class="titulo">Botillería La Lucy</p>
						<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
					</div>
				</a>

				<!-- Contacto #2 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
						</div>
				</a>

				<!-- Contacto #3 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
						</div>
				</a>

				<!-- Contacto #4 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
						</div>
				</a>

			</div>
		</div>

	</div>
<?php require 'footer.php'; ?>